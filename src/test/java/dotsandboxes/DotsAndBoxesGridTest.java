package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    // FIXME: You need to write tests for the two known bugs in the code.
    @Test
    public void testBoxComplete() {
        logger.info("Creating a new game");
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(3, 5, 1);

        logger.info("Drawing three of four box sides");
        grid.drawHorizontal(0,0,1);
        grid.drawHorizontal(1, 0, 1);
        grid.drawVertical(0, 0, 1);

        assertFalse(grid.boxComplete(0,0));

        logger.info("Completing the box");
        grid.drawVertical(0, 1, 1);

        assertTrue(grid.boxComplete(0,0));
    }

    @Test
    public void testLineDoubleUp() {
        logger.info("Creating a new game");
        DotsAndBoxesGrid gridTwo = new DotsAndBoxesGrid(3, 5, 1);

        logger.info("Drawing a horizontal line");
        gridTwo.drawHorizontal(1, 1, 1);
        assertThrows(IllegalStateException.class, () -> gridTwo.drawHorizontal(1, 1, 1), "This horizontal line is already taken");

        logger.info("Drawing a vertical line");
        gridTwo.drawVertical(2, 2, 1);
        assertThrows(IllegalStateException.class, () -> gridTwo.drawVertical(2, 2, 1), "This vertical line is already taken");
    }

}
